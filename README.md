# Only HTML and CSS Slideshow

## Summary

This exercise is based in https://www.youtube.com/watch?v=9Irz0c-6UGw

It uses just CSS and HTML to create a functional and fancy slideshow.


## Improvements:

- CSS variables used to easily change colors of the elements in the UI and the size of the slideshow.


## Keypoints

- A radio input option controls which slide to see.
- The slides container is 100x % wide, with x, the number of slides. The trick is to move the left margin of the first slide according to the input value.


## Conclusions

- Intereting exercise to view a simple to use slideshow without worrying about JS code.
- Less JS code to test.
- It works only for scenarios where you know the number of slides from design and it is hard to apply in most scenarios, where the number of slides may vary.
